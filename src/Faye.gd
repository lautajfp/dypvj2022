extends KinematicBody2D


export (int) var speed = 1200
export (int) var jump_speed = -1800
export (float, 0, 1.0) var friction = 0.1
export (float, 0, 1.0) var acceleration = 0.25
export (int) var gravity = 4000

# Player stats
export (int) var health_max = 100
export (int) var health_regeneration = 1
var health = health_max

# Other
var direction = 0
var last_direction = 1.0
var velocity = Vector2.ZERO
var attack_playing = false
var facing = "right"
var animation = "idle"
var on_air = false

# Signals
signal player_interacts
signal player_stats_changed

func animates_player(velocity: Vector2, last_facing: String, animation: String):
	if velocity.x > 1:
		facing = "right"
	elif velocity.x < -1:
		facing = "left"
	else:
		facing = last_facing
	if on_air and velocity.y < -2:
		animation = "land"
	animation = animation + "_" + facing
	$AnimatedSprite.play(animation)



func get_input(last_facing: String) -> Dictionary:
	# Horizontal movement
	direction = 0
	if is_on_floor():
		on_air = false
	if Input.is_action_pressed("move_right"):
		direction += 1
		facing = "right"
		animation = "walk"
	if Input.is_action_pressed("move_left"):
		direction -= 1
		facing = "left"
		animation = "walk"
	if !(Input.is_action_pressed("move_right") or Input.is_action_pressed("move_left")):
		facing = last_facing
		animation = "idle"
	if direction != 0:
		velocity.x = lerp(velocity.x, direction * speed, acceleration)
	else:
		velocity.x = lerp(velocity.x, 0, friction)
	
	# Vertical movement
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			velocity.y = jump_speed
			facing = last_facing
			animation ="jump"
			on_air = true
	
	# Other
	if Input.is_action_pressed("interact"):
		emit_signal("player_interacts")
	
	return {"direction": direction, "facing": facing, "animation": animation}


func hit(damage):
	health -= damage
	emit_signal("player_stats_changed", self)


func _physics_process(delta):
	var last_facing = facing
	var animation_based_on_input = get_input(last_facing)
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	animates_player(velocity, last_facing, animation_based_on_input.animation)
