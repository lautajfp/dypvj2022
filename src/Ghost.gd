extends KinematicBody2D

# References
var player

# Ghost stats
var health = 100
var health_max = 100
var health_regeneration = 1

# Attack variables
var attack_damage = 10
var attack_cooldown_time = 1500
var next_attack_time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
		player = get_tree().root.get_node("Root/Faye")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var target = $RayCast2D.get_collider()
	if target != null and target.name == "Player" and player.health > 0:
		player.hit(attack_damage)
