extends Control


# Called when the node enters the scene tree for the first time.


func _on_Play_pressed():
	yield(get_tree(), "idle_frame")
	var _change_scene = get_tree().change_scene("res://src/Cutscene.tscn")


func _on_Exit_pressed():
	get_tree().quit()
