extends Sprite


export (String) var next_level = null 
var portal: Sprite
var is_on_portal: bool = false


# Called when the node enters the scene tree for the first time.
func _ready():
	portal = get_tree().root.get_node("Root/Portal")


func _on_Area2D_body_entered(body):
	if body.name == "Faye":
		portal.scale = Vector2(1.1, 1.1)
		is_on_portal = true
	


func _on_Area2D_body_exited(body):
	if body.name == "Faye":
		portal.scale = Vector2(1.0, 1.0)
		is_on_portal = false


func _on_Faye_player_interacts():
	if is_on_portal:
		yield(get_tree(), "idle_frame")
		var _change_scene = get_tree().change_scene("res://src/" + next_level + ".tscn")
	else:
		pass
